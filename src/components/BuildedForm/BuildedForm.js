import styles from './BuildedForm.module.scss';

const BuildedForm = ({ input, textarea, checkbox }) => {
	const elements = (dataType) => {
		let type = '';
		switch (dataType) {
			case input:
				type = 'text';
				break;
			case textarea:
				type = 'textarea';
				break;
			case checkbox:
				type = 'checkbox';
				break;
			default:
				break;
		}

		if (dataType.checked && dataType.value > 0) {
			const elems = [];
			for (let i = 0; i < dataType.value; i++) {
				elems.push(
					<div key={i}>
						<input type={type} />
					</div>
				);
			}
			return elems;
		}
	};

	return (
		<form className={styles.builded}>
			{elements(input)}
			{elements(textarea)}
			{elements(checkbox)}
		</form>
	);
};

export default BuildedForm;
