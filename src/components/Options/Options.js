import { useState } from 'react';
import BuildedForm from '../BuildedForm';
import styles from './Options.module.scss';

const Options = () => {
	const [dataInputs, setDataInputs] = useState({
		input: {
			checked: false,
			value: '',
		},
		textarea: {
			checked: false,
			value: '',
		},
		checkbox: {
			checked: false,
			value: '',
		},
	});

	const [buildedForm, setBuildedForm] = useState(null);

	const onBuildForm = (e) => {
		e.preventDefault();
		setBuildedForm(<BuildedForm {...dataInputs} />);
	};

	const onInputChange = (e, name) => {
		const inp = e.target;
		setDataInputs((prev) => {
			const result = JSON.parse(JSON.stringify(prev));

			if (inp.type === 'checkbox') {
				result[name].checked = inp.checked;
			} else {
				let val = parseInt(inp.value);
				if (val) {
					result[name].value = val;
				}
			}

			return result;
		});
	};

	return (
		<>
			<form className={styles.optionForm}>
				<div>
					<input
						onChange={(e) => onInputChange(e, 'input')}
						type="checkbox"
						checked={dataInputs.input.checked}
					/>
					<label>Input</label>
					<input
						onChange={(e) => onInputChange(e, 'input')}
						type="text"
						value={dataInputs.input.value}
					/>
				</div>
				<div>
					<input
						onChange={(e) => onInputChange(e, 'textarea')}
						type="checkbox"
						checked={dataInputs.textarea.checked}
					/>
					<label>Textarea</label>
					<input
						onChange={(e) => onInputChange(e, 'textarea')}
						type="text"
						value={dataInputs.textarea.value}
					/>
				</div>
				<div>
					<input
						onChange={(e) => onInputChange(e, 'checkbox')}
						type="checkbox"
						checked={dataInputs.checkbox.checked}
					/>
					<label>Checkbox</label>
					<input
						onChange={(e) => onInputChange(e, 'checkbox')}
						type="text"
						value={dataInputs.checkbox.value}
					/>
				</div>

				<div>
					<button onClick={onBuildForm}>Build</button>
				</div>
			</form>
			{buildedForm}
		</>
	);
};

export default Options;
