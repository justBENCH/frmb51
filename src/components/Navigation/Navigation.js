import { Link } from 'react-router-dom';
import styles from './Navigation.module.scss';

const Navigation = () => {
	return (
		<div className={styles.nav}>
			<Link to="/form">
				<button type="button">FORM</button>
			</Link>
		</div>
	);
};

export default Navigation;
