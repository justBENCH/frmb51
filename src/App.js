import { BrowserRouter as Router, Route } from 'react-router-dom';
import PageForm from './pages/form';
import Navigation from './components/Navigation';

const App = () => {
	return (
		<Router>
			<Route path="/" exact component={Navigation} />
			<Route path="/form" component={PageForm} />
		</Router>
	);
};

export default App;
